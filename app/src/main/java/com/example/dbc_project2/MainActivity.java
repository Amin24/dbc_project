package com.example.dbc_project2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.dbc_project2.QR_Generator.GeneratorActivity;
import com.example.dbc_project2.Scanner.ScannerActivity;

public class MainActivity extends AppCompatActivity {

    Button button;
    ImageButton buttonqr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.buttonhori);
        buttonqr = (ImageButton) findViewById(R.id.qr1);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, GeneratorActivity.class));
            }
        });

        buttonqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ScannerActivity.class));
            }
        });
    }
}
